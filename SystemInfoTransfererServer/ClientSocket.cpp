/*

Lee Hae Chang

PC System Information Transferer with MFC

*/

// ClientSocket.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "SystemInfoTransfererServer.h"
#include "ClientSocket.h"
#include "ListenSocket.h"
#include "SystemInfoTransfererServerDlg.h"

// CClientSocket

CClientSocket::CClientSocket()
{
	m_pListenSocket = NULL;
}

CClientSocket::~CClientSocket()
{
}

// CClientSocket 멤버 함수

void CClientSocket::SetListenSocket(CAsyncSocket *pSocket) {
	m_pListenSocket = pSocket;
}

void CClientSocket::OnClose(int nErrorCode)
{
	CSocket::OnClose(nErrorCode);

	CString strTmp, strIPAddress;
	UINT uPortNumber;
	CListenSocket *pServerSocket = (CListenSocket*)m_pListenSocket;
	CSystemInfoTransfererServerDlg *pMain = (CSystemInfoTransfererServerDlg*)AfxGetMainWnd();

	GetPeerName(strIPAddress, uPortNumber);

	strTmp.Format(_T("[%s:%d]가 접속을 종료했습니다."), strIPAddress, uPortNumber);
	pMain->m_List.AddString(strTmp);
	pMain->m_List.SetCurSel(pMain->m_List.GetCount() - 1);

	pServerSocket->CloseClientSocket(this);
}

void CClientSocket::OnReceive(int nErrorCode)
{
	CString strTmp = _T(""), strIPAddress = _T("");
	UINT uPortNumber = 0;
	TCHAR szBuffer[1024];
	::ZeroMemory(szBuffer, sizeof(szBuffer));

	GetPeerName(strIPAddress, uPortNumber);

	if (Receive(szBuffer, sizeof(szBuffer)) > 0) {
		CSystemInfoTransfererServerDlg *pMain = (CSystemInfoTransfererServerDlg*)AfxGetMainWnd();
		rapidjson::Document document;
		char tempStr[1024];
		int len = 0;

		for (int i = 0; szBuffer[i] != '\0'; i++, len++) {
			tempStr[i] = szBuffer[i];
		}

		tempStr[len] = '\0';

		document.Parse(tempStr);

		if (document["windowsVersion"].IsString()) {
			CString str(document["windowsVersion"].GetString());

			message[59].strWindowsVersion = str;
		}
		if (document["numOfProcess"].IsString())
		{
			CString str(document["numOfProcess"].GetString());

			message[59].nNumOfProcess = _ttoi(str);
		}
		if (document["cpuUsage"].IsString()) {
			CString str(document["cpuUsage"].GetString());

			message[59].fCPUUsage = _ttof(str);
		}
		if (document["memTotal"].IsString()) {
			CString str(document["memTotal"].GetString());

			message[59].dMemTotal = _wtof(str);
		}
		if (document["availMemTotal"].IsString()) {
			CString str(document["availMemTotal"].GetString());

			message[59].dAvailMemTotal = _wtof(str);
		}
		if (document["virtualTotal"].IsString()) {
			CString str(document["virtualTotal"].GetString());

			message[59].dVirtualTotal = _wtof(str);
			message[59].dMemInUse = message[59].dMemTotal - message[59].dAvailMemTotal;
		}

		pMain->DrawCPUGraph(message);
		pMain->DrawMemoryGraph(message);
		pMain->Invalidate(FALSE);

		for (int i = 0; i <= 58; i++) {
			message[i] = message[i + 1];
		}

		strTmp.Format(_T("[%s:%d]: 운영체제: %s, 프로세서 수: %d, cpu 사용률: %.1f, 메모리 전체 크기(MB): %d, 사용 가능 메모리(MB): %d, 가상 메모리(MB): %d")
			, strIPAddress, uPortNumber, message[59].strWindowsVersion, message[59].nNumOfProcess, message[59].fCPUUsage
			, (int)message[59].dMemTotal, (int)message[59].dAvailMemTotal, (int)message[59].dVirtualTotal);
		pMain->m_List.AddString(strTmp);
		pMain->m_List.SetCurSel(pMain->m_List.GetCount() - 1);
	}

	CSocket::OnReceive(nErrorCode);
}