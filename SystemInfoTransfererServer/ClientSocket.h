/*

Lee Hae Chang

PC System Information Transferer with MFC

*/

#pragma once
#pragma warning(disable:4996)

#include "ListenSocket.h"
#include "ReceiveMessage.h"
#include "rapidjson\stringbuffer.h"
#include "rapidjson\document.h"
#include "rapidjson\writer.h"

// CClientSocket 명령 대상입니다.

class CClientSocket : public CSocket
{
public:
	CClientSocket();
	virtual ~CClientSocket();
	void SetListenSocket(CAsyncSocket *pSocket);

public:
	CAsyncSocket *m_pListenSocket;
	virtual void OnClose(int nErrorCode);
	virtual void OnReceive(int nErrorCode);

public:
	RM message[60];
};