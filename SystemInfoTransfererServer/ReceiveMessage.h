#pragma once

#include "stdafx.h"

typedef struct ReceiveMessage {
	CString strWindowsVersion = nullptr;
	UINT nNumOfProcess = 0;
	double dMemTotal = -1;
	double dAvailMemTotal = -1;
	double dMemInUse = -1;
	double dVirtualTotal = -1;
	float fCPUUsage = -1;
} RM;