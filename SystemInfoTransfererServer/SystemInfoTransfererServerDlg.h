/*

Lee Hae Chang

PC System Information Transferer with MFC

*/

// SystemInfoTransfererServerDlg.h : 헤더 파일
//

#pragma once
#include "ListenSocket.h"
#include "ClientSocket.h"
#include "ReceiveMessage.h"

#define GRAPH_WIDTH 420.0
#define GRAPH_HEIGHT 120.0
#define CPUGRAPHTOPY 380
#define MEMORYGRAPHTOPY 570

// CSystemInfoTransfererServerDlg 대화 상자
class CSystemInfoTransfererServerDlg : public CDialogEx
{
// 생성입니다.
public:
	CSystemInfoTransfererServerDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_SYSTEMINFOTRANSFERERSERVER_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedClose();
	DECLARE_MESSAGE_MAP()
public:
	void DrawCPUGraph(RM *message);
	void DrawMemoryGraph(RM *message);
public:
	CListBox m_List;
	CListenSocket m_ListenSocket;
	CWinThread *pThread;
	CClientSocket *pClientSocket;
	CRect m_DlgSize;
	bool m_bIsDrawing;
};