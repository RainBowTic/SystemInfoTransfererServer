/*

Lee Hae Chang

PC System Information Transferer with MFC

*/

#pragma once

// CListenSocket 명령 대상입니다.

class CListenSocket : public CAsyncSocket
{
public:
	CListenSocket();
	virtual ~CListenSocket();
	virtual void OnAccept(int nErrorCode);
	void CloseClientSocket(CSocket *pClient);

public:
	CPtrList m_ptrClientSocketList;
};