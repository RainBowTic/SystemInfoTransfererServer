/*

Lee Hae Chang

PC System Information Transferer with MFC

*/

// ListenSocket.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "SystemInfoTransfererServer.h"
#include "ListenSocket.h"
#include "ClientSocket.h"
#include "SystemInfoTransfererServerDlg.h"

// CListenSocket

CListenSocket::CListenSocket()
{
}

CListenSocket::~CListenSocket()
{
}

// CListenSocket 멤버 함수

void CListenSocket::OnAccept(int nErrorCode)
{
	CClientSocket *pClient = new CClientSocket();

	if (Accept(*pClient)) {
		CString strTmp = _T(""), strIPAddress = _T("");
		UINT uPortNumber = 0;
		CSystemInfoTransfererServerDlg *pMain = (CSystemInfoTransfererServerDlg*)AfxGetMainWnd();

		pClient->SetListenSocket(this);
		m_ptrClientSocketList.AddTail(pClient);
		pClient->GetPeerName(strIPAddress, uPortNumber);

		strTmp.Format(_T("[%s:%d]가 접속했습니다."), strIPAddress, uPortNumber);
		pMain->m_List.AddString(strTmp);
		pMain->m_List.SetCurSel(pMain->m_List.GetCount() - 1);
	}
	else {
		delete pClient;
		AfxMessageBox(_T("ERROR: Failed to accept new client!"));
	}

	CAsyncSocket::OnAccept(nErrorCode);
}

void CListenSocket::CloseClientSocket(CSocket *pClient) {
	POSITION pos;
	pos = m_ptrClientSocketList.Find(pClient);

	if (pos != NULL) {
		if (pClient != NULL) {
			pClient->ShutDown();
			pClient->Close();
		}

		m_ptrClientSocketList.RemoveAt(pos);
		delete pClient;
	}
}