/*

Lee Hae Chang

PC System Information Transferer with MFC

*/

// SystemInfoTransfererServerDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "SystemInfoTransfererServer.h"
#include "SystemInfoTransfererServerDlg.h"
#include "afxdialogex.h"
#include "ReceiveMessage.h"
//#include "ClientSocket.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

	// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CSystemInfoTransfererServerDlg 대화 상자



CSystemInfoTransfererServerDlg::CSystemInfoTransfererServerDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_SYSTEMINFOTRANSFERERSERVER_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CSystemInfoTransfererServerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_List);
}

BEGIN_MESSAGE_MAP(CSystemInfoTransfererServerDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDCLOSE, &CSystemInfoTransfererServerDlg::OnBnClickedClose)
END_MESSAGE_MAP()


// CSystemInfoTransfererServerDlg 메시지 처리기

BOOL CSystemInfoTransfererServerDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다.  응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	GetWindowRect(&m_DlgSize);
	pThread = nullptr;
	m_bIsDrawing = false;
	pClientSocket = new CClientSocket();

	if (m_ListenSocket.Create(21000, SOCK_STREAM)) {
		if (!m_ListenSocket.Listen()) {
			AfxMessageBox(_T("ERROR: Listen() return FALSE"));
		}
	}
	else {
		AfxMessageBox(_T("ERROR: Failed to create server socket!"));
	}

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CSystemInfoTransfererServerDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다.  문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CSystemInfoTransfererServerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CSystemInfoTransfererServerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CSystemInfoTransfererServerDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	POSITION pos;
	pos = m_ListenSocket.m_ptrClientSocketList.GetHeadPosition();
	CClientSocket *pClient = NULL;

	while (pos != NULL) {
		pClient = (CClientSocket*)m_ListenSocket.m_ptrClientSocketList.GetNext(pos);

		if (pClient != NULL) {
			pClient->ShutDown();
			pClient->Close();

			delete pClient;
		}
	}

	m_ListenSocket.ShutDown();
	m_ListenSocket.Close();
}

void CSystemInfoTransfererServerDlg::OnBnClickedClose()
{
	m_ListenSocket.ShutDown();
	m_ListenSocket.Close();

	PostQuitMessage(0);
}

void CSystemInfoTransfererServerDlg::DrawCPUGraph(RM *message) {
	CDC *pDC = GetDC();
	CPen *oldPen;
	CPen newPen;
	int graphStartX = (m_DlgSize.right - m_DlgSize.left - (GRAPH_WIDTH + GRAPH_WIDTH / 60)) / 2;
	int graphStartY = CPUGRAPHTOPY;
	int graphEndX = graphStartX + GRAPH_WIDTH;
	int graphEndY = graphStartY + GRAPH_HEIGHT;

	pDC->SetBkMode(TRANSPARENT); // 글씨 배경 투명
	pDC->TextOutW(graphStartX, graphStartY - 30, _T("CPU 사용률(%)"));

	// 흰색 사각형 바탕 칠하기
	pDC->Rectangle(graphStartX, graphStartY, graphEndX + GRAPH_WIDTH / 60, graphEndY);

	// 파란색 사각형 틀 그리기
	newPen.CreatePen(PS_SOLID, 2, RGB(38, 168, 217));
	oldPen = pDC->SelectObject(&newPen);

	pDC->MoveTo(graphStartX, graphStartY);
	pDC->LineTo(graphStartX, graphEndY);
	pDC->LineTo(graphEndX + GRAPH_WIDTH / 60, graphEndY);
	pDC->LineTo(graphEndX + GRAPH_WIDTH / 60, graphStartY);
	pDC->LineTo(graphStartX, graphStartY);

	pDC->TextOutW(graphStartX, graphEndY + 10, _T("60초"));
	pDC->TextOutW(graphEndX, graphEndY + 10, _T("1초"));

	// 눈금선 그리기
	CPen linePen;
	linePen.CreatePen(PS_SOLID, 1, RGB(255, 128, 192));
	pDC->SelectObject(linePen);

	for (int i = 1; i <= 9; i++) {
		pDC->MoveTo(graphStartX, graphStartY + GRAPH_HEIGHT / 10 * i);
		pDC->LineTo(graphEndX + GRAPH_WIDTH / 60, graphStartY + GRAPH_HEIGHT / 10 * i);
	}

	linePen.DeleteObject();

	// 그래프 그리기
	for (int i = 0; i < 60; i++) {
		if (message[i].dMemInUse > 0) {
			int x = (GRAPH_WIDTH / 60) * (i + 1) + graphStartX;
			double lineLength = message[i].fCPUUsage*GRAPH_HEIGHT / 100;
			int y = graphEndY - lineLength;

			pDC->SelectObject(&newPen);
			pDC->MoveTo(x, graphEndY);
			pDC->LineTo(x, y);
		}
	}

	pDC->SelectObject(oldPen);

	newPen.DeleteObject();

	ReleaseDC(pDC);
}

void CSystemInfoTransfererServerDlg::DrawMemoryGraph(RM *message) {
	CDC *pDC = GetDC();
	CPen *oldPen;
	CPen newPen;
	int graphStartX = (m_DlgSize.right - m_DlgSize.left - (GRAPH_WIDTH + GRAPH_WIDTH / 60)) / 2;
	int graphStartY = MEMORYGRAPHTOPY;
	int graphEndX = graphStartX + GRAPH_WIDTH;
	int graphEndY = graphStartY + GRAPH_HEIGHT;

	pDC->SetBkMode(TRANSPARENT); // 글씨 배경 투명
	pDC->TextOutW(graphStartX, graphStartY - 30, _T("메모리 사용률(%)"));

	// 흰색 사각형 바탕 칠하기
	pDC->Rectangle(graphStartX, graphStartY, graphEndX + GRAPH_WIDTH / 60, graphEndY);

	// 빨간색 사각형 틀 그리기
	newPen.CreatePen(PS_SOLID, 2, RGB(220, 10, 104));
	oldPen = pDC->SelectObject(&newPen);

	pDC->MoveTo(graphStartX, graphStartY);
	pDC->LineTo(graphStartX, graphEndY);
	pDC->LineTo(graphEndX + GRAPH_WIDTH / 60, graphEndY);
	pDC->LineTo(graphEndX + GRAPH_WIDTH / 60, graphStartY);
	pDC->LineTo(graphStartX, graphStartY);

	pDC->TextOutW(graphStartX, graphEndY + 10, _T("60초"));
	pDC->TextOutW(graphEndX, graphEndY + 10, _T("1초"));

	// 눈금선 그리기
	CPen linePen;
	linePen.CreatePen(PS_SOLID, 1, RGB(116, 222, 220));
	pDC->SelectObject(linePen);

	for (int i = 1; i <= 9; i++) {
		pDC->MoveTo(graphStartX, graphStartY + GRAPH_HEIGHT / 10 * i);
		pDC->LineTo(graphEndX + GRAPH_WIDTH / 60, graphStartY + GRAPH_HEIGHT / 10 * i);
	}

	linePen.DeleteObject();

	// 그래프 그리기
	for (int i = 0; i < 60; i++) {
		if (message[i].dMemInUse > 0) {
			int x = (GRAPH_WIDTH / 60) * (i + 1) + graphStartX;
			double lineLength = (message[i].dMemInUse / message[i].dMemTotal)*GRAPH_HEIGHT;
			int y = graphEndY - lineLength;

			pDC->SelectObject(&newPen);
			pDC->MoveTo(x, graphEndY);
			pDC->LineTo(x, y);
		}
	}

	newPen.DeleteObject();

	ReleaseDC(pDC);
}